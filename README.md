# ideato

this website is constantly under development.

## Project setup

run this command before starting the project in order to install all the depedecies.

```
yarn install
```

### Compiles and hot-reloads for development

this runs a local server on http://localhost:8080 for development.

```
yarn serve
```

### Compiles and minifies for production

run this for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

made with ♥ by Mostafa Rahmati.
