import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueCookies from "vue-cookies";
import * as Sentry from "@sentry/browser";
import { Vue as VueIntegration } from "@sentry/integrations";

Vue.config.productionTip = false;
Sentry.init({
  dsn:
    "https://1fb9d25deab946a5aef98c6a1fac2552@o397832.ingest.sentry.io/5252747",
  integrations: [new VueIntegration({ Vue, attachProps: true })],
});
Vue.use(VueCookies);
new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
