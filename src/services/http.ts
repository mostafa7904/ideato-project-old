export class HTTP {
  url: string;
  method: string;
  data: object;
  headers: HeadersInit;
  constructor(url: string, method: string, data: object, headers: HeadersInit) {
    this.url = url;
    this.data = data;
    this.method = method;
    this.headers = headers;
  }

  send() {
    return fetch(this.url, {
      method: this.method,
      headers: this.headers,
      body: JSON.stringify(this.data),
    })
      .then((res) => res.json())
      .catch((e) => {
        console.error(e);
      });
  }
}
